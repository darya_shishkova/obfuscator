import java.util.Scanner;

public class MultiTask {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        System.out.print("Input the first multiplier: ");
        int a = reader.nextInt();
        System.out.print("Input the second multiplier: ");
        int b = reader.nextInt();
        multiplication(a, b);
    }

    /**
     * performs multiplication of two integers
     * @param a multiplier
     * @param b multiplier
     */
    private static void multiplication(int a, int b) {
        int sum = 0;
        if(a == 0 || b == 0){
            answerOutput(sum);
        }else {
            int modA = Math.abs(a);
            int modB = Math.abs(b);
            if(modB > modA) {
                int i = modA;//intermediate variable
                modA = modB;// must be maximal!!!
                modB = i;
            }//modA > modB!!!
            for (int i = 0; i < modB; i++) {
                sum = sum + modA;
            }
            if ((b < 0 && a > 0) || (a < 0 && b > 0)){
                sum = -sum;
            }
            answerOutput(sum);
        }
    }
    private static void answerOutput(int sum) {
        System.out.println("The Answer is: " + sum);
    }
}
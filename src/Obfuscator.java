import java.io.*;
import java.util.ArrayList;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Obfuscator {
    public static void main(String[] args) {
        String path = "src//MultiTask.java";
        File file = new File(path);
        String className = file.getName().replace(".java", "");
        String newClassName = String.valueOf(getRandomClassName());
        ArrayList<String> myFile = readMyFile(path);
        myFile = deleteComments(myFile);
        myFile = withoutSpaces(myFile);
        StringBuilder stringBuilder = toOneLine(myFile);
        String renamed = renameClass(stringBuilder, className, newClassName);
        writeMyResults(renamed, newClassName);
    }

    /**
     * Считывает строки из указанного файла и записывает их в ArrayList
     *
     * @param path путь к файлу
     * @return ArrayList, содержащий считанные из файла строки
     */
    private static ArrayList<String> readMyFile(String path) {
        ArrayList<String> strings = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(path))) {
            String string;
            while ((string = bufferedReader.readLine()) != null) {
                strings.add(string);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return strings;
    }

    /**
     * Записывает строку в файл в папку src с указанным именем и расширением .java
     *
     * @param string       строка
     * @param newClassName новое имя файла
     */
    private static void writeMyResults(String string, String newClassName) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("src//" + newClassName + ".java"))) {
            bufferedWriter.write(string);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Удаляет комментарии
     *
     * @param strings ArrayList со считанными из файла строками
     * @return новый ArrayList со строками из файла, не содержащий в себе комментариев
     */
    private static ArrayList<String> deleteComments(ArrayList<String> strings) {
        String temp; //временная перменная для строки
        ArrayList<String> noComments = new ArrayList<>();
        boolean isComment = false;
        for (int i = 0; i < strings.size(); i++) {
            temp = strings.get(i);
            if (!isComment) {
                for (int j = 0; j < temp.length() - 1; j++) {
                    if (temp.charAt(j) == '/' && temp.charAt(j + 1) == '*') {
                        isComment = true;
                    }
                }
            }
            if (isComment) {
                for (int j = 0; j < temp.length() - 1; j++) {
                    if (temp.charAt(j) == '*' && temp.charAt(j + 1) == '/') {
                        isComment = false;
                        i++;
                    }
                }
            }
            if (!isComment) {
                if (strings.get(i).contains("//")) {
                    String[] stringParts = strings.get(i).split("//");
                    noComments.add(stringParts[0]);
                } else {
                    noComments.add(strings.get(i));
                }
            }
        }
        return noComments;
    }

    /**
     * Удаляет ненужные пробелы
     *
     * @param strings ArrayList со считанными из файла строками
     * @return новый ArrayList со строками из файла, не содержащий в себе лишних пробелов
     */
    private static ArrayList<String> withoutSpaces(ArrayList<String> strings) {
        ArrayList<String> withoutSpaces = new ArrayList<>();
        Pattern pattern = Pattern.compile("( \\W{1,3})|( \\W{1,3} )|(\\W{1,3} )");
        for (String content : strings) {
            content = content.replaceAll("\\s{2,}", "");
            Matcher matcher = pattern.matcher(content);
            while (matcher.find()) {
                content = content.replace(matcher.group(), matcher.group().trim());
            }
            withoutSpaces.add(content);
        }
        return withoutSpaces;
    }

    /**
     * Записывает содержимое ArrayList'a в одну строку
     *
     * @param strings ArrayList со считанными из файла строками
     * @return строки из файла, объединненые в одну строку
     */
    private static StringBuilder toOneLine(ArrayList<String> strings) {
        StringBuilder oneLine = new StringBuilder();
        for (String string : strings) {
            oneLine.append(string);
        }
        oneLine = new StringBuilder(oneLine.toString().replaceAll(" {2}", ""));
        return oneLine;
    }

    /**
     * Генерирует случайный символ для нового имени класса
     *
     * @return новое односимвольное имя класса
     */
    private static char getRandomClassName() {
        Random random = new Random();
        String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        return alphabet.charAt(random.nextInt(alphabet.length()));
    }

    /**
     * Переименовывает класс и конструкторы
     *
     * @param string  строка, содержащая обфусцированный код
     * @param oldName старое имя класса/конструктора
     * @param newName новое имя класса/конструктора
     * @return строка, содержащая обфусцированный код с измененным именем класса и именем конструкторов
     */
    private static String renameClass(StringBuilder string, String oldName, String newName) {
        String renamedString = String.valueOf(string);
        return renamedString.replaceAll(oldName, newName);
    }
}
